import tensorflow as tf

g = tf.Graph()

with g.as_default() as g:
    tf.train.import_meta_graph('training/model.ckpt-200.meta')

with tf.Session(graph=g) as sess:
    file_writer = tf.summary.FileWriter(logdir='checkpoint_log_dir/registration_plate_v1', graph=g)
